# LYrA-flutter

Project lyrics AFAFI, made with flutter LYrA (LYRics Afafi).
A.FA.Fi is a choir in FPVM church Madagascar.
<p> 
This project is dokerized in order to facilitate the installation. You can look at the file images/Dockerfile, it contains the dependencies installed and I have created an image with it.
<br>
On the other hand, the file Dockerfile in the main folder will be used to call the image. 
</p>

## Getting started
<p>
To launch this project under docker you should install docker if you don't have it. Please install it from the official document. <a href="https://docs.docker.com/engine/install/">Official documentation</a>
</p>

## Set up VS Code
<p>
- Download Visual Studio Code;<br>
- Download extensions:<br>
<ul>
    <li>Docker <a href="https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker">link</a></li>
    <li>Remote Development <a href="https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack">link</a></li>
</ul>
- Build and run Docker container<br>
   <ul>
   <li>
     Select the option Remote-containers: Open folder in container
   </li>
   <li>
   Select the root directory which contains the Dockerfile
   </li>
   </ul>
   After the build finishes, you will be in the bash of container.
</p>

## Run the app
<p>
I used a connected device to test the app.<br>
- run flutter doctor to check if your phone is connected to the container
- flutter run to launch the application
</p>




