import 'dart:async';

import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'package:path_provider/path_provider.dart';

/// https://github.com/JohannesMilke/sqflite_database_example/blob/master/lib/db/notes_database.dart
class LyraDb {
  LyraDb._init();
  static final LyraDb instance = LyraDb._init();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await _initDb('lyra.db');

    return _database;
  }

  Future<Database> _initDb(String dbFileName) async {
    WidgetsFlutterBinding.ensureInitialized();
    io.Directory applicationDirectory =
        await getApplicationDocumentsDirectory();
    String dbPath = path.join(applicationDirectory.path, dbFileName);

    return await openDatabase(dbPath, version: 1, onCreate: _createTable);
  }

  Future _createTable(Database db, int version) async {
    await db.execute('''
    CREATE TABLE IF NOT EXISTS lyrics(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      id_gva_lyric INTEGER NOT NULL,
      code TEXT NOT NULL,
      author TEXT,
      key TEXT,
      lyric TEXT NOT NULL,
      title TEXT,
      type INTEGER NOT NULL
    )
    ''');
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }

  void initializeTableLyrics(db, snapshotDocs) async {
    Batch batch = db.batch();
    await db.execute('''
        DELETE FROM lyrics
        ''');
    int type = 0;
    for (var doc in snapshotDocs) {
      if (doc['type'] % 2 == 0) {
        type = 1;
      } else {
        type = 0;
      }
      batch.insert(
          "lyrics",
          {
            'id_gva_lyric': doc['id'],
            'code': doc['code'],
            'author': doc['author'] ?? "",
            'key': doc['key'] ?? "",
            'lyric': doc['lyric'],
            'title': doc['title'],
            'type': type
          },
          conflictAlgorithm: ConflictAlgorithm.replace);
    }
    await batch.commit();
  }
}
