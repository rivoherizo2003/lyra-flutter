import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:lyra/models/lyric.dart';
import 'package:lyra/service/lyric_service.dart';
import 'package:lyra/shared/widget/list_tile_lyric.dart';

class GlaPage extends StatefulWidget {
  const GlaPage({Key key}) : super(key: key);

  @override
  _GlaPageState createState() => _GlaPageState();
}

class _GlaPageState extends State<GlaPage> {
  List<Lyric> lyrics = [];
  List<Lyric> resultSearch = [];
  bool isLoading = false;
  SearchBar searchBar;

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: const Text('Gala'),
      actions: [searchBar.getSearchAction(context)],
    );
  }

  void onChangedSearch(String value) async {
    final lyrics_ = await LyricService.instance.searchLyric(0, value);
    setState(() => resultSearch = lyrics_);
  }

  @override
  void initState() {
    super.initState();

    init();
  }

  Future init() async {
    final lyrics_ = await LyricService.instance.getAllLyricsByType(0);
    setState(() {
      resultSearch = lyrics_;
      lyrics = lyrics_;
    });
  }

  _GlaPageState() {
    searchBar = SearchBar(
      setState: setState,
      buildDefaultAppBar: buildAppBar,
      inBar: false,
      onChanged: onChangedSearch,
      onCleared: () => resultSearch = lyrics,
      onClosed: () => resultSearch = lyrics,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: searchBar.build(context),
      body: ListView.builder(
        itemCount: resultSearch.length,
        itemBuilder: (context, index) {
          return lyricTyle(resultSearch[index], context, index);
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          setState(() {
            resultSearch = [];
          });
          final lyrics_ = await LyricService.instance.getAllLyricsByType(0);
          setState(() {
            resultSearch = lyrics_;
          });
        },
        backgroundColor: Colors.pink,
        child: const Icon(Icons.refresh),
      ),
    ));
  }
}
