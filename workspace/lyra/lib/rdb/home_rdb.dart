import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:lyra/models/lyric.dart';
import 'package:lyra/service/lyric_service.dart';
import 'package:lyra/shared/widget/list_tile_lyric.dart';

class RdbPage extends StatefulWidget {
  const RdbPage({Key key}) : super(key: key);
  @override
  _RdbPageState createState() => _RdbPageState();
}

class _RdbPageState extends State<RdbPage> {
  List<Lyric> lyrics = [];
  List<Lyric> resultSearch = [];
  bool isLoading = false;
  SearchBar searchBar;

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: const Text('Rodobe'),
      actions: [searchBar.getSearchAction(context)],
    );
  }

  void onChangedSearch(String value) async {
    final lyrics_ = await LyricService.instance.searchLyric(1, value);
    setState(() {
      resultSearch = lyrics_;
    });
  }

  void getAllLyricsRdbByType() async {
    final lyrics_ = await LyricService.instance.getAllLyricsByType(1);
    setState(() {
      lyrics = lyrics_;
    });
  }

  @override
  void initState() {
    super.initState();
    init();
  }

  Future init() async {
    final lyrics_ = await LyricService.instance.getAllLyricsByType(1);
    setState(() {
      resultSearch = lyrics_;
      lyrics = lyrics_;
    });
  }

  _RdbPageState() {
    searchBar = SearchBar(
      setState: setState,
      buildDefaultAppBar: buildAppBar,
      inBar: false,
      onChanged: onChangedSearch,
      onCleared: () => resultSearch = lyrics,
      onClosed: () => resultSearch = lyrics,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: searchBar.build(context),
      body: ListView.builder(
        itemCount: resultSearch.length,
        itemBuilder: (context, index) {
          return lyricTyle(resultSearch[index], context, index);
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          setState(() {
            resultSearch = [];
          });
          final lyrics_ = await LyricService.instance.getAllLyricsByType(1);
          setState(() {
            resultSearch = lyrics_;
          });
        },
        backgroundColor: Colors.green,
        child: const Icon(Icons.refresh),
      ),
    ));
  }
}
