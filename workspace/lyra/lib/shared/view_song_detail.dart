import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:lyra/models/lyric.dart';
import 'package:lyra/service/lyric_service.dart';
import 'package:lyra/shared/lyric_param.dart';

class ViewSongDetail extends StatefulWidget {
  const ViewSongDetail({Key key}) : super(key: key);
  @override
  _ViewSongDetail createState() => _ViewSongDetail();
}

class _ViewSongDetail extends State<ViewSongDetail> {
  @override
  Widget build(BuildContext context) {
    final lyricParam = ModalRoute.of(context).settings.arguments as LyricParam;

    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: Text(lyricParam.title),
            ),
            body: FutureBuilder<Lyric>(
                future: LyricService.instance.findOneBy(lyricParam.id),
                builder: (BuildContext context, AsyncSnapshot<Lyric> snapShot) {
                  Lyric lyric = snapShot.data;
                  if (lyric != null) {
                    return SingleChildScrollView(
                        child: Card(
                            child: Column(children: <Widget>[
                      Html(data: lyric.lyric, style: {
                        "div": Style(
                            fontSize: FontSize.rem(1.3),
                            lineHeight: LineHeight.rem(0.7)),
                        "p": Style(
                            fontSize: FontSize.rem(1.3),
                            lineHeight: LineHeight.rem(1.5))
                      })
                    ])));
                  } else {
                    return const Center(child: Text("no detail"));
                  }
                })));
  }
}
