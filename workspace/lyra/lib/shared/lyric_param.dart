class LyricParam {
  final int id;
  final String title;

  LyricParam({this.id, this.title});
}
