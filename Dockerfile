FROM ozirehdocker/oz-flutter:1.0


ENV PATH "$PATH:/home/developer/.pub-cache/bin"

# Run basic check to download Dark SDK 
RUN flutter doctor --android-licenses

USER root
